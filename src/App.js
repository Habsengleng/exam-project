import React, { Component } from "react";
import Productlist from "./Components/Productlist";

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      products: [
        {
          id: 1,
          name: "Carabao",
          importDate: "09-03-2020",
          isHiding: false,
        },
        {
          id: 2,
          name: "Pepsi",
          importDate: "10-01-2020",
          isHiding: false,
        },
        {
          id: 3,
          name: "Samurai",
          importDate: "12-03-2020",
          isHiding: false,
        },
        {
          id: 4,
          name: "Coca Cola",
          importDate: "12-04-2020",
          isHiding: false,
        },
        {
          id: 5,
          name: "Bacchus",
          importDate: "12-04-2019",
          isHiding: false,
        },
      ],
    };
  }
  unhide = (id) => {
    this.setState({
      isHiding: true,
    });
  };
  render() {
    return (
      <div className="container">
        <div className="row">
          <Productlist products={this.state.products} onUnhide={this.unhide} />
        </div>
      </div>
    );
  }
}
