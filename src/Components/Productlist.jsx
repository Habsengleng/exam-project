import React from "react";
import { Table, Button } from "react-bootstrap";

export default function Productlist(props) {
  let products = props.products.map((product) => (
    <tr key={product.id}>
      <td>{product.id}</td>
      <td>{product.name}</td>
      <td>{product.importDate}</td>
      <td>
        <Button variant="danger" onClick={()=>}>
          Hide
        </Button>
      </td>
    </tr>
  ));
  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>ImportDate</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>{products}</tbody>
    </Table>
  );
}
